# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#20.times do |i|
#  rand = (Random.new.rand*10).to_i
#  q_need = [0,1,2,3].shuffle.first
#  CreditCard.create(name: "prod-test#{i}", fee: rand, apr_min: rand, apr_max: rand+10, description: "prod-test",
#                    quality_need: q_need, application_url: "http://www.google.com", loan_application_url: "http://www.google.com")
#
#end


business_type_names = [
  "Auto Dealer & Sales",
  "Bars & Lounges",
  "Chiropractors",
  "Construction-related",
  "Contractors (HVAC, Plumbing, Other)",
  "Convenience Stores, Markets & Delis",
  "Education, Schools & Daycare",
  "Hotels & Motels",
  "Furniture & Home Furnishings",
  "Gas Stations",
  "Laundry & Dry Cleaning Services",
  "Manufacturing",
  "Media & Communications",
  "Medical Professionals / Doctors",
  "Online & Home-based Businesses",
  "Professional Services (Law, Accounting, Other)",
  "Real Estate",
  "Restaurants & Cafes",
  "Retail Businesses (Clothing, General, Other)",
  "Salons & Spas",
  "Service Providers",
  "Skin Care / Laser Clinics",
  "Travel & Transportation Services",
  "Veterinary",
  "Wholesale & Distribution",
  "Wine & Liquor Stores",
  "Other"
]

BusinessType.delete_all
business_type_names.each{ |name| BusinessType.create(name: name)}

years_in_business = [
  "New / Start-up",
  "1 month - 3 months",
  "3 months - 1 year",
  "1 year - 5 years",
  "5+ years"
]

BusinessYear.delete_all
years_in_business.each{ |name| BusinessYear.create(name: name) }

monthly_sales = [
  "Greater than $100,000",
  "$50,000 - $100,000",
  "$20,000 - $50,000",
  "$5,000 - $20,000",
  "Less than $5,000",
  "Current Monthly Sales"
]

MonthlySale.delete_all
monthly_sales.each{ |name| MonthlySale.create(name: name) }

priorities = [
  "Low Annual Fees",
  "Most Amount of Funds",
  "Lowest Interest Rate"
]

Priority.delete_all
priorities.each{ |name| Priority.create(name: name) }

rewards_programs = [
  "Travel Rewards Programs",
  "Other Rewards/Points",
  "Cashback Rewards"
]

RewardProgram.delete_all
rewards_programs.each{ |name| RewardProgram.create(name: name) }

credit_scores = [
  "Bad (350 - 619)",
  "Fair (620 - 659)",
  "Good (660 - 749)",
  "Excellent (750 - 850)"
]

CreditScore.delete_all
credit_scores.each{ |name| CreditScore.create(name: name) }