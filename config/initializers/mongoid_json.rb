module Mongoid
  module Document
    def as_json(options={})
      attrs = super(options)
      attrs["id"] = attrs["_id"]
      attrs
    end
  end
end

module BSON
  class ObjectId
    def as_json(options={})
      to_s
    end
    def to_json(*a)
      to_s
    end
  end
end
