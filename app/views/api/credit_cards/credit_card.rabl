attributes :name, :description, :application_url, :loan_application_url, :apr_min, :apr_max, :fee, :quality_need, :business_accept_cards
node(:credit_score_ids){|card| card.credit_score_ids.map(&:to_s)}
node(:business_type_ids){|card| card.business_type_ids.map(&:to_s)}
node(:monthly_sale_ids){|card| card.monthly_sale_ids.map(&:to_s)}
node(:priority_ids){|card| card.priority_ids.map(&:to_s)}
node(:reward_program_ids){|card| card.reward_program_ids.map(&:to_s)}
node(:business_year_ids){|card| card.business_year_ids.map(&:to_s)}
node(:id){|card| "#{card._id}"}
node(:_id){|card| "#{card._id}"}
node(:image_urls){|card| card.image_url}
