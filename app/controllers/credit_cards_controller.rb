class CreditCardsController < MainController
  before_filter :parse_seach_params
  def index
    @search = CreditCard.search(@search_params)
    @credit_cards = @search.collection
  end

  private
  def parse_seach_params
    @search_params = {}
    rules = {
      business_type: /business-type-(.+)/,
      credit_score: /credit-score-(.+)/,
      business_year: /years-in-business-(.+)/,
      monthly_sale: /monthly-sale-(.+)/,
      priority: /credit-card-type-priority-(.+)/,
      reward_program: /preferred-rewards-program-(.+)/,
      business_accept_cards: /business-accepts-credit-cards-(.+)/
    }
    pars = params.select{|k,v| k =~ /f\d+/}.values
    pars += params[:path].split("_") if params[:path]
    pars.each do |value|
      rules.each do |name, reg|
        value =~ reg
        @search_params[name] = $1 if $1
      end
    end
  end
end
