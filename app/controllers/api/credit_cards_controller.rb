class Api::CreditCardsController < Api::BaseController
  before_filter :authenticate_api_user!, except: [:index, :show]

  def index
    @credit_cards = CreditCard.page(params[:page]).per(10)
    respond_with :api, @credit_cards
  end

  def show
    @credit_card = CreditCard.find(params[:id])
    respond_with :api, @credit_card
  end

  def create
    credit_card = CreditCard.new(credit_card_params)
    credit_card.save
    respond_with :api, credit_card
  end

  def update
    credit_card = CreditCard.find(params[:id])
    if credit_card.update_attributes(credit_card_params)
      render status: 200, json: credit_card.to_json
    else
      render status: 422, json: { errors: credit_card.errors }
    end
  end

  def destroy
    CreditCard.find(params[:id]).destroy
    respond_to do |format|
      format.json { render json: {notice: "CreditCard was successfully deleted!"} }
    end
  end

  private
  def credit_card_params
    params.require(:credit_card).permit(:name, :description, :quality_need, :application_url,
                                        :loan_application_url, :apr_min, :apr_max, :image_data, :business_accept_cards, credit_score_ids:[], business_year_ids:[],
                                        reward_program_ids:[], priority_ids: [], monthly_sale_ids: [], business_type_ids: [])
  end
end
