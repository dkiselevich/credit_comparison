class Api::ListsController < Api::BaseController

  def index
    render json: {
                    credit_scores: CreditScore.recent,
                    monthly_sales: MonthlySale.recent,
                    rewards_program: RewardProgram.recent,
                    business_types: BusinessType.recent,
                    business_years: BusinessYear.recent,
                    priorities: Priority.recent
                 }
  end
end
