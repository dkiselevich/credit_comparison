class Api::BaseController < ActionController::Base
  respond_to :json
  before_filter :authenticate_api_user!
end