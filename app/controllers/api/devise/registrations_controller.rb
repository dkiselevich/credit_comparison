class Api::Devise::RegistrationsController < Devise::RegistrationsController
  respond_to :json
  def create
    resource = User.new(user_params)
    if resource.save
      if resource.active_for_authentication?
        sign_up(resource_name, resource)
        respond_with resource, location: "/profile"
      else
        expire_session_data_after_sign_in!
        respond_with resource, location: "/login"
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  private
  def user_params
    params[:api_user] ||= {}
    params[:api_user].permit(:first_name, :last_name, :password, :password_confirmation, :email)
  end
end