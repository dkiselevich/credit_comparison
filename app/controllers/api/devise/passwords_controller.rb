class Api::Devise::PasswordsController < Devise::PasswordsController
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    if successfully_sent?(resource)
      render status: 200, json: { info: "instructions were sent to email" }
    else
      render status: 422, json: { errors: resource.errors, info: "wrong params for password reset" }
    end
  end

  def update
    self.resource = resource_class.reset_password_by_token(resource_params)
    if resource.errors.empty?
      resource.unlock_access! if unlockable?(resource)
      sign_in(resource_name, resource)
      render status: 200, json: { info: "password was reseted" }
    else
      render status: 422, json: { errors: resource.errors, info: "wrong params for password reset" }
    end
  end
end