class Api::Devise::SessionsController < Devise::SessionsController
  respond_to :json
  before_filter :ward_auth

  def create
    if current_api_user
      respond_with current_api_user, location: "profile"
    else
      authentication_failure
    end
  end

  def destroy
    sign_out
    render status: 200, json: { info: "Logged Out" }
  end

  def show_current_user
    if current_api_user
      render status: 200, json: { info: "Current User", user: current_api_user }
    else
      render status: 401, json: { info: "Not authorized" }
    end
  end

  def facebook_auth
    fb_user = FacebookUser.new(params[:authResponse])
    if fb_user.valid?
      sign_in(fb_user.get_user)
      render status: 200, json: { info: "User logged in through facebook successfully" }
    else
      render status: 422, json: { info: "Failed auth from facebook", errors: fb_user.errors }
    end
  end

  private
  def ward_auth
    warden.authenticate(scope: resource_name, recall: "#{controller_path}#failure")
  end

  def authentication_failure
    render status: 422, json: { success: false, errors: "Error with your login or password" }
  end
end