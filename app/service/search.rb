class Search
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  attr_accessor :collection, :attributes

  def initialize(options={})
    self.attributes = options
    options.each do |k,v|
      self.class.send("attr_accessor", k)
      self.send("#{k}=",v)
    end
  end

  def method_missing(name)
    ""
  end

  def persisted?
    false
  end
end
