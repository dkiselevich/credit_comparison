window.main.factory "railsErrorParse", ->
  (val) ->
    if val? and val isnt []
      val.join(", ")
    else
      ""
window.main.directive "errorField", (railsErrorParse) ->
  restrict: "E"
  scope:
    error: "="
  replace: true
  template: '<div class = "has-error alert alert-danger" ng-show="hasError"> {{errors}} </div>'
  link: (scope, element, attr) ->
    scope.hasError = false
    scope.$watch "error", ->
      if scope.error?
        scope.errors = railsErrorParse(scope.error)
        scope.hasError = true
      else
        scope.hasError = false
