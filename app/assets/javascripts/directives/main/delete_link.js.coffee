main.directive "delete", ->
  scope:
    delete: "="
    hide: "@"

  link: (scope, element, atts) ->
    element.bind "click", ->
      scope.$apply ->
        scope.delete["_destroy"] = true
      element.closest(".#{scope.hide}").hide()

