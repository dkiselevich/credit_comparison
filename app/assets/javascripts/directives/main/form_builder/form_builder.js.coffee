
window.main.factory "inputBuilder", ["simpleBuilder", (simpleBuilder)->
  class inputBuilder extends simpleBuilder

    generateTemplate: ->
      "<input class = \"form-control\" ng-model = \"#{@model}.#{@attribute}\" placeholder=\"#{@placeholder}\" #{@additionalOptions}>"

  inputBuilder
]

window.main.factory "errorBuilder", ["simpleBuilder", (simpleBuilder)->
  class errorBuilder extends simpleBuilder

    generateTemplate: ->
      "<error-field errors = \"{{#{@model}.errors.#{@attribute}}}\"></error-field>"
  errorBuilder
]

window.main.factory "simpleBuilderConfig", ->
  class simpleBuilderConfig
    @input_wrapper: (wrapperName) ->
      if @wrappers[wrapperName]?.input_wrapper?
        @wrappers[wrapperName]?.input_wrapper?
      else
        @wrappers["standart"].input_wrapper

    @wrappers:
      standart:
        input_wrapper: angular.element("<div></div>")


  simpleBuilderConfig

window.main.factory "simpleBuilder", ->
  class simpleBuilder
    constructor: (@attrs) ->
      @parsedAtts = {}
      @additionalOptions = ""
      @parseAtts()


    build: ->
      angular.element(@generateTemplate())

    generateTemplate: ->
      console.log "override this method"

    parseAtts: ->
      @parseModelAndAttrbiute()
      @parseIncludedAtts()
      @parsePlaceholder()

    parseIncludedAtts: ->
      atts = @attrs.$attr
      for attr, value of atts when !(attr in @atts_to_skip)
        @parsedAtts[value] = @attrs[value]
        @additionalOptions += "#{attr} = \"#{value}\' "

    parsePlaceholder: ->
      @placeholder = if @parsedAtts["placeholder"]?
        @parsedAtts["placeholder"]
      else
        @snakeToHuman(@attribute)

    parseModelAndAttrbiute: ->
      @model = @attrs.model
      @attribute = @attrs.attr

    snakeToHuman: (snakeName)->
      transformed = snakeName.replace(/([A-Z])/g, " $1")
      transformed.charAt(0).toUpperCase() + transformed.slice(1)

    atts_to_skip: ["model", "attr", "label", "simple-input"]
  simpleBuilder

window.main.directive "simpleInput", (inputBuilder, errorBuilder, simpleBuilderConfig, $compile) ->
  replace: true
  restrict: "EA"
  transclude: false
  template:""
  link: (scope, tElement, attrs) ->
    wrapper = ($compile("<div></div>")(scope))
    input_builder = new inputBuilder(attrs)
    input = $compile(input_builder.build())(scope);
    wrapper.append(input)
    error_builder = new errorBuilder(attrs)
    error = $compile(error_builder.build())(scope);
    wrapper.append(error)
    tElement.replaceWith(wrapper)


