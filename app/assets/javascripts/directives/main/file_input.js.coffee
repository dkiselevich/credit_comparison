main.directive "fileinput", ->
  scope:
    fileinput: "="
    afterLoad: "&"
    onLoad: "&"
  link: (scope, element, attributes) ->
    element.bind "change", (changeEvent) ->
      reader = new FileReader()
      reader.onload = (loadEvent) ->
        file = loadEvent.target.result
        scope.$apply ->
          scope.fileinput = file
          scope.onLoad?({file: file})
        preview = attributes.previewContainer
        if preview
          $(preview).attr("src", file)
      reader.readAsDataURL(changeEvent.target.files[0])
      scope.afterLoad?()

