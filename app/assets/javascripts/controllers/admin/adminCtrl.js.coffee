@adminCtrl = (Devise, $location, $state, $scope)->
  $scope.session = new Devise.session

  $scope.session.requestCurrentUser().then (resp) ->
    if $state.current.name is "admin"
      $state.go("admin.cards.index", {page: 1})
    $scope.authenticated = true
  , ->
    $state.go("devise.login")
