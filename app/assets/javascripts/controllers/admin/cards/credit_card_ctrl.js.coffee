@creditCardCtrl = ($scope, creditCard, $state, Lists)->
  Lists.query().then (options)->
    $scope.lists = options

  if $state.current.name is "admin.card.new"
    $scope.card = new creditCard
  else
    creditCard.get($state.params.cardName).then (card)->
      $scope.card = card

  $scope.save = ->
    $scope.card.save().then (success)->
      if $scope.card.valid()
        $state.go("admin.cards.index")