@allCreditCardsCtrl = ($scope, creditCard, Paginator, $stateParams, $state)->
  page = if $stateParams?.page?
            $stateParams?.page
         else
            1

  creditCard.query({page: page}).then (response)->
    $scope.cards = response.creditCards
    $scope.paginator = new Paginator(response.total, response.page, 5, $state)

  $scope.destroy = (card_params) ->
    if confirm("Sure you want to delete credit card '#{card_params.name}'?")
      card = new creditCard(card_params)
      card.delete().then ->
        index = $scope.cards.indexOf(card_params)
        $scope.cards.splice(index, 1)
