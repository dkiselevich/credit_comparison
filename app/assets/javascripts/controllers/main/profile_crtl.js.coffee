@profileCtrl = ($scope, currentUser, Devise)->
  $scope.langs = ["ru", "en"]
  $scope.genders = ["male", "female"]
  $scope.editingPhoto = false

  session = new Devise.session

  session.requestCurrentUser().then (resp)->
    $scope.authenticated = true
  , ->
    $location.path "/login"

  currentUser.get().then (user) ->
     $scope.user = user

  $scope.updatePhoto = ->
    $scope.user.update().then ->
        $scope.user.editingPhoto = false
        $scope.user.photoUrls.thumb = $scope.user.photoUrls.thumb + "#"+ new Date().getTime()

