main.controller "wineFormCtrl", ($scope, Devise, CellarData, $modal, $modalInstance, currentUser, $window, $http) ->
  currentUser.get().then (user)->
    $scope.user = user

  $scope.data = CellarData

  $scope.currentTab = ''

  $scope.toggleTab = (tab) ->
    $scope.currentTab = if $scope.currentTab == tab then '' else tab

  $scope.openedTab = (tab) ->
    tab == $scope.currentTab

  $scope.collectErrors = (data) ->
    $scope.errors = {}
    for field, value of data
      $scope.errors[field] = value.join(", ")

  $scope.saveWine = ->
    CellarData.wine.save().then ->
      if CellarData.wine.isValid()
        CellarData.wine.reloadMarker()
        $modalInstance.close()
        CellarData.loadWines()
      else
        $scope.collectErrors(CellarData.wine.errors)


  $scope.deleteWine = ->
    if window.confirm("Are you sure?")
      CellarData.wine.delete().then (answer) ->
        CellarData.removeMarker()
        CellarData.loadWines()

      $modalInstance.close()

  $scope.closeDialog = ->
    $modalInstance.close()

  $scope.action = ->
    if $scope.data.wine.isNew()
      "Add"
    else
      "Edit"

  $scope.submitText = ->
    if $scope.data.wine.isNew()
      "Add Wine"
    else
      "Save"

  $scope.postFB = ->
    FB.ui
      method: "feed"
      link: "http://r.dev-better.com/"
      name: "myWei"
      caption: 'Wine Experience!'
      description: $scope.tweetMessage()
      redirect_uri: "http://r.dev-better.com/"
    , (response) ->
      alert "Message was posted to your feed!"

  $scope.postTweet = ->
    $window.open("https://twitter.com/intent/tweet?status=#{$scope.tweetMessage()}","","width=600,height=400")

  $scope.tweetMessage = ->
    message = "#{$scope.user.firstName} #{$scope.user.lastName} "
    message += "drank #{$scope.data.wine.name}"


  $scope.countryChange = ->
    CellarData.wine.regionId = ""
    CellarData.wine.subRegionId = ""
    CellarData.loadRegions()
    CellarData.updateNames()

  $scope.regionChange = ->
    CellarData.wine.subRegionId = ""
    CellarData.loadSubRegions()
    CellarData.updateNames()

  $scope.subRegionChange = ->
    CellarData.updateNames()

  $scope.addExpPhoto = (file)->
    $scope.data.wine.experience = {} unless $scope.data.wine.experience?
    $scope.data.wine.experience.photos = [] unless  $scope.data.wine.experience.photos?
    $scope.data.wine.experience.photos.push({photoData: file})


  $scope.select2Ajax =
    placeholder: "Search for a movie"
    minimumInputLength: 1
    query: (query) ->
      data = {results: []}
      $http.get("/api/autocomplete_places?query=#{query.term}").then (response) ->
        data.results = response.data
        query.callback(data)

    initSelection: (element, callback)->
      callback(element.val())
    formatResult: (place)->
      "<div>" + place+ "</div>"
    id: (place) ->
      place
    formatSelection:  (place)->
      "<div>" + place+ "</div>"
