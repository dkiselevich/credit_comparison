@mainCtrl = (Devise, $location, $state)->
  session = new Devise.session
  session.requestCurrentUser().then ->
    $state.go "global.cellar", {},
      location: false
