@facebookAuthCtrl = ($scope, Facebook, Session)->
  $scope.facebook_session = Session
  $scope.face_book = Facebook

  $scope.$watch ->
    Facebook.isReady()
  , (newVal)->
    $scope.facebookReady = true

  $scope.login = ->
    $scope.face_book.login (response) ->
      $scope.facebook_session.facebook_auth(response)
    , scope: "email"