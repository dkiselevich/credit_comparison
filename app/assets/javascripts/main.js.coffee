# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#= require_self
#= require_tree ./services
#= require_tree ./controllers/main
#= require_tree ./controllers/admin
#= require_tree ./directives/main
#= require_tree ./config

window.main = angular.module('main',['deviceService', 'facebook', 'rails', 'xeditable', 'ui.bootstrap',
                                     "ui.select2", 'ui.router', "angularFileUpload","checklist-model"])

main.config ['$locationProvider', ($locationProvider) ->
  $locationProvider.html5Mode(true).hashPrefix('!');
]