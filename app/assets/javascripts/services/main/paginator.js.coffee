main.factory "Paginator", ->
  class Paginator
    constructor: (@total, @page, @inline=5, @state)->
    pages: ->
      offset = parseInt(@inline/2)

      start = if (@page - offset) > 0
                @page - offset
              else
                1

      end = if (@page + offset) < @total
              if @page + offset < @inline
                @inline
              else
                @page + offset
            else
              start = if start - (@page + offset - @total) > 0
                        start - (@page + offset - @total)
                      else
                        1

              @total
      [start..end]

main.directive "ngPaginate", ->
  restrict: "A"
  scope:
    paginator: "="
  template: "<ul class='pagination point'><li ng-repeat='i in paginator.pages()' data-id='{{i}}' page-link ng-class='{active: isActive({{i}})}'><a>{{i}}</a></li></ul>"

main.directive "pageLink", ($state)->
  restrict: "A"
  link: (scope, el, attr, ngPaginateCtrl)->
    scope.isActive = (el)->
      el == parseInt(scope.paginator.page)
    el.bind "click", ->
      scope.$apply ->
        $state.go(scope.paginator.state.current.name, {page: attr.id})

