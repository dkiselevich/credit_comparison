main.factory "Lists", (RailsResource, railsSerializer) ->
  class Lists extends RailsResource
    @configure
      url: '/api/lists'
