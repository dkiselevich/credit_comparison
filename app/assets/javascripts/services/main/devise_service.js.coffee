angular.module("deviceService", ["facebook"]).factory "Devise", ($location, $http, $q, $state, Facebook) ->
  class Devise
    errors: null

    setErrors: (data)->
      @errors = {}
      for field, value of data
        @errors[field] = value.join(", ")

    errorFullMessage: (attr)->
      if @errors && @errors[attr]?
        "#{attr}: #{@errors[attr]}"
      else
        null

    redirect: (url) ->
      url = url or "/"
      $location.path url

    setState: (state, location) ->
      state = state or "home"
      $state.go(state, {}, {location: location})


  class Password extends Devise
    passPath: "/api/devise/password"

    recover: ->
      $http.post( @passPath,
                 api_user:
                  email: @email
      ).then  =>
        @recovered = true
      , (error) =>
        @setErrors(error.data.errors)

    reset: ->
      $http.put( @passPath,
                 api_user:
                  password: @password
                  password_confirmation: @password_confirmation
                  reset_password_token: @reset_password_token

      ).then  =>
        @redirect("/admin")
      , (error) =>
        @setErrors(error.data.errors)

  class Session extends Devise
    profile_path: "/admin"

    login: ->
      $http.post("/api/devise/sign_in",
                 api_user:
                   email: @email
                   password: @password
      ).then (success) =>
        @currentUser = success.data
      ,(error) =>
        @errors = error.data.errors

    logout: (redirectTo) ->
      Facebook.logout()
      $http({method: 'DELETE', url: "/api/devise/sign_out"}).then =>
        @currentUser = null
        @setState("devise.login", true)

    register: (user) ->
      $http.post("/api/devise",
                 api_user:
                  user
      ).then =>
          @setState("global.cellar", false)
        ,(error) =>
          @setErrors(error.data.errors)

    facebook_auth: (omni_params) ->
      $http.post("/api/devise/facebook_auth", omni_params).then (success) =>
        @setState("global.cellar", false)
      , (error) =>
        @setErrors(error.data.errors)

    requestCurrentUser: ->
      defer = $q.defer()
      $http.get("/api/devise/current_user").then (response) =>
          @currentUser = response.data.user
          @currentUser
          defer.resolve()
        , (error) =>
          defer.reject()
      defer.promise

    currentUser: null

    isAuthenticated: ->
      !!@currentUser


  class DeviseService
    @session: Session
    @password: Password

  DeviseService
