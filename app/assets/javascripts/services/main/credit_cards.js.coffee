main.factory "creditCard", (RailsResource, railsSerializer) ->
  class creditCardResource extends RailsResource
    @configure
      url: '/api/credit_cards/{{id}}'
      name: 'credit_card'
      serializer: railsSerializer ->
        @.exclude('errors')
      responseInterceptors:[ (promise) ->
          promise.then (response) ->
            if angular.isArray(response.data) and angular.isDefined(response.originalData.count)
              response.data.$count = response.originalData.count
              response.data.$page = response.originalData.page
            response
      ]
    valid: ->
      jQuery.isEmptyObject(@errors)
