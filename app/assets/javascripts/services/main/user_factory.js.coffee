window.main.factory "User", (railsResourceFactory) ->
  railsResourceFactory
    url: "api/users"
    name: "user"