class CreditCard
  include Mongoid::Document
  include Mongoid::Slug
  field :name, type: String
  include MetaSearchMongoid
  slug :name
  field :description, type: String
  field :quality_need, type: Integer
  field :application_url, type: String
  field :loan_application_url, type: String
  field :apr_min, type: Integer
  field :apr_max, type: Integer
  field :fee, type: Integer
  field :business_accept_cards, type: Boolean, default: false

  has_and_belongs_to_many :business_types
  has_and_belongs_to_many :credit_scores
  has_and_belongs_to_many :monthly_sales
  has_and_belongs_to_many :priorities
  has_and_belongs_to_many :reward_programs
  has_and_belongs_to_many :business_years

  validates :name, uniqueness: true, on: :create
  validates :name, :description, :quality_need, :application_url, :loan_application_url, presence:  true
  validates :fee, numericality: true, allow_blank: true
  validates :apr_max, :apr_min, numericality: true
  validates :application_url, :loan_application_url, url: true
  mount_uploader :image, CardImageUploader, type: String

  def self.search(options={})
    options ||= {}
    options = options.select{|k,v| !v.blank?}
    search = Search.new(options)
    search.collection = searcher(options)
    search
  end

  def self.searcher(options)
    chain = self.scoped
    options.each do |name, val|
      chain = chain.send("by_#{name}", val)
    end
    chain
  end

  def self.by_business_type(val)
    self.where(:business_type_ids => BusinessType.find(val).id)
  end

  def self.by_business_year(val)
    self.where(:business_year_ids => BusinessYear.find(val).id)
  end

  def self.by_monthly_sale(val)
    self.where(:monthly_sale_ids => MonthlySale.find(val).id)
  end

  def self.by_priority(val)
    self.where(:priority_ids => Priority.find(val).id)
  end

  def self.by_credit_score(val)
    self.where(:credit_score_ids => CreditScore.find(val).id)
  end

  def self.by_reward_program(val)
    self.where(:reward_program_ids => RewardProgram.find(val).id)
  end

  def self.by_business_accept_cards(val)
    self.where(:business_accept_cards => val)
  end

  def quality_name
    credit_scores.pluck(:name).join(", ")
  end

  def image_data=(data)
    io = CarrierStringIO.new(Base64.decode64(data.gsub(/.*base64,/, '')))
    self.image = io
  end
end
