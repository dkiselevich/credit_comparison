class List
  include Mongoid::Document
  include Mongoid::Slug
  include Mongoid::Timestamps
  field :name, type: String
  slug :name
  scope :recent, ->{ order_by(:created_at => :asc) }
end
