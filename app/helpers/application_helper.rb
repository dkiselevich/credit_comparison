module ApplicationHelper
  def credit_card_fee(card)
    card.fee || "N/A"
  end

  def filters(search)
    atts = []
    search.attributes.each do |key, value|
      atts << "#{t(key.to_s)} : #{get_value(key,value)}"
    end
    atts.join(" | ")
  end

  def get_value(key, value)
    case key
    when :business_accept_cards
      value == "true" ? "yes" : "no"
    else
      key.to_s.classify.constantize.find(value).name
    end
  end
end
